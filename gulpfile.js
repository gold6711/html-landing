'use strict';

// Подключение плагинов через переменные (connection of plugins through variables):
var gulp = require('gulp'), // Gulp
    autoprefixer = require('gulp-autoprefixer'), // Добавление вендорных префиксов (adding of vendor prefixers)
    concat = require('gulp-concat'), // Объединение файлов (files merger)
    csso = require('gulp-csso'), // Минификация CSS-файлов (minification of CSS files)
    del = require('del'), // Удаление папок и файлов (delete of folders and files)
    imagemin = require('gulp-imagemin'), // Оптимизация изображений (images optimization)
    plumber = require('gulp-plumber'), // Обработка ошибок (error handling)
    pngquant = require('imagemin-pngquant'), // Оптимизация PNG-изображений (PNG images optimization)
    pug = require('gulp-pug'), // Pug
    rename = require('gulp-rename'), // Переименование файлов (files rename)
    stylus = require('gulp-stylus'), // Stylus
    uglify = require('gulp-uglify'); // Минификация JS-файлов (minification of JS files)

// Задание путей к используемым файлам и папкам (paths to folders and files):
var paths = {
    dir: {
        app: './app',
        dist: './dist'
    },
    watch: {
        html: './app/pug/**/*.pug',
        css: [
            './app/blocks/**/*.styl',
            './app/config/**/*.styl',
            './app/lessons/**/*.styl'
        ],
        js: './app/blocks/**/*.js'
    },
    app: {
        html: {
            src: './app/pug/pages/*.pug',
            dest: './app'
        },
        common: {
            css: {
                src: [
                    './app/config/variables.styl',
                    './app/blocks/**/*.styl'
                ],
                dest: './app/assets/css'
            },
            js: {
                src: './app/blocks/**/*.js',
                dest: './app/assets/js'
            }
        },
        media: {
            css: {
                src: './app/blocks/media/*.styl',
                dest: './app/assets/css'
            }
        },
        lesson: {
            css: {
                src: [
                    './app/lessons/**/*.styl',
                    './app/config/variables.styl'
                    ],
                dest: './app/assets/lesson-css'
            }
        },
        vendor: {
            css: {
                src: [
                    './app/vendor/normalize-css/normalize.css',
                    //'./app/vendor/bootstrap/dist/css/bootstrap.min.css',
                    './bootstrap/css/bootstrap.min.css'

                ],
                dest: './app/assets/css'
            },
            js: {
                src: [
                    //'./app/vendor/bootstrap/dist/js/bootstrap.min.js',
                    './bootstrap/js/bootstrap.min.js'
                ],
                dest: './app/assets/js'
            }
        }
    },
    img: {
        src: './app/assets/images/**/*.*',
        dest: './dist/assets/images'
    },
    dist: {
        html: {
            src: './app/*.html',
            dest: './dist'
        },
        css: {
            src: './app/assets/css/*.min.css',
            dest: './dist/assets/css'
        },
        lcss: {
            src: './app/assets/lesson-css/*.css',
            dest: './dist/assets/lesson-css'
        },
        js: {
            src: './app/assets/js/*.min.js',
            dest: './dist/assets/js'
        }

    }
}

// Подключение Browsersync (connection of Browsersync):
var browserSync = require('browser-sync').create(),
    reload = browserSync.reload;

// Таск для работы Browsersync, автообновление браузера (Browsersync task, autoreload of browser):
gulp.task('serve', function() {
    browserSync.init({
        server: './app'
    });
    gulp.watch(paths.watch.html, gulp.series('html'));
    gulp.watch(paths.watch.css, gulp.series('cssCommon'));
    gulp.watch(paths.watch.css, gulp.series('cssMedia'));
    gulp.watch(paths.watch.css, gulp.series('cssLesson'));
    gulp.watch(paths.watch.js, gulp.series('jsCommon'));
    gulp.watch('*.html').on('change', reload);
});

// Таск для работы Pug, преобразование Pug в HTML (Pug to HTML conversion task):
gulp.task('html', function () {
    return gulp.src(paths.app.html.src)
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(paths.app.html.dest))
        .pipe(browserSync.stream());
});

// Таск для преобразования Stylus-файлов в CSS (Stylus to CSS conversion):
gulp.task('cssCommon', function() {
    return gulp.src(paths.app.common.css.src)
        .pipe(plumber())
        .pipe(concat('common.styl'))
        .pipe(stylus())
        .pipe(autoprefixer({browsers: ['last 16 versions']}))
        .pipe(gulp.dest(paths.app.common.css.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(csso())
        .pipe(gulp.dest(paths.app.common.css.dest))
        .pipe(browserSync.stream());
});

gulp.task('cssMedia', function() {
    return gulp.src(paths.app.media.css.src)
        .pipe(plumber())
        .pipe(concat('media.styl'))
        .pipe(stylus())
        .pipe(autoprefixer({browsers: ['last 2 versions']}))
        .pipe(gulp.dest(paths.app.media.css.dest))
        .pipe(browserSync.stream());
});

gulp.task('cssLesson', function() {
    return gulp.src(paths.app.lesson.css.src)
        .pipe(plumber())
        .pipe(concat('lesson.styl'))
        .pipe(stylus())
        .pipe(autoprefixer({browsers: ['last 2 versions']}))
        .pipe(gulp.dest(paths.app.lesson.css.dest))
        .pipe(browserSync.stream());
});

// Таск для объединения и минификации пользовательских JS-файлов (task for merger and minification custom JS files)
gulp.task('jsCommon', function() {
    return gulp.src(paths.app.common.js.src)
        .pipe(plumber())
        .pipe(concat('common.js'))
        .pipe(gulp.dest(paths.app.common.js.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.app.common.js.dest))
        .pipe(browserSync.stream());
});

// Таск для объединения и минификации CSS-файлов внешних библиотек (task for merger and minification CSS files of libraries, plugins and frameworks)
gulp.task('cssVendor', function () {
    return gulp.src(paths.app.vendor.css.src)
        .pipe(concat('vendor.min.css'))
        .pipe(csso())
        .pipe(gulp.dest(paths.app.vendor.css.dest));
});

// Таск для объединения и минификации JS-файлов внешних библиотек (task for merger and minification JS files of libraries, plugins and frameworks)
gulp.task('jsVendor', function () {
    return gulp.src(paths.app.vendor.js.src)
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.app.vendor.js.dest));
});

// fonts here

// Таск для предварительной очистки (удаления) production-папки (task for delete of production folder dist):
gulp.task('clean', function() {
    return del(paths.dir.dist);
});

// Таск для обработки изображений (images optimization task):
gulp.task('img', function() {
    return gulp.src(paths.img.src)
        .pipe(imagemin({use: [pngquant()]}))
        .pipe(gulp.dest(paths.img.dest));
});

// Таск для формирования production-папки (task for creating of production folder dist):
gulp.task('dist', function () {
    var htmlDist = gulp.src(paths.dist.html.src)
        .pipe(gulp.dest(paths.dist.html.dest));
    var cssDist = gulp.src(paths.dist.css.src)
        .pipe(gulp.dest(paths.dist.css.dest));
    var lcssDist = gulp.src(paths.dist.lcss.src)
        .pipe(gulp.dest(paths.dist.lcss.dest));
    var jsDist = gulp.src(paths.dist.js.src)
        .pipe(gulp.dest(paths.dist.js.dest));
    return htmlDist, cssDist, lcssDist, jsDist;
});

// Таск для сборки (build task):
gulp.task('build', gulp.parallel('html', 'cssCommon', 'cssMedia', 'jsCommon', 'cssVendor', 'jsVendor'));

// Таск для разработки (development task):
gulp.task('default', gulp.series('build', 'serve'));

// Таск для production (production task):
gulp.task('public', gulp.series('clean', 'img', 'dist'));



// ********  SMART GRID   *********  //

var smartgrid = require('smart-grid');

/* It's principal settings in smart grid project */
var settings = {
    outputStyle: 'styl', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '30px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    container: {
        maxWidth: '1200px', /* max-width Ð¾n very large screen */
        fields: '30px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1100px' /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '960px'
        },
        sm: {
            width: '780px',
            fields: '15px' /* set fields only if you want to change container.fields */
        },
        xs: {
            width: '560px'
        }
        /*
         We can create any quantity of break points.

         some_name: {
         width: 'Npx',
         fields: 'N(px|%|rem)',
         offset: 'N(px|%|rem)'
         }
         */
    }
};

smartgrid('app/lessons/grid-stylus', settings);